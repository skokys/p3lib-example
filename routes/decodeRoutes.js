const express = require('express')
const ammc = require('ammc-wasm')
const router = express.Router()

router.post('/', function(req, res, next) {
  const binaryData = req.body.data

  const decoded = ammc.to_json(binaryData)

  res.send(decoded);
});

module.exports = router;
