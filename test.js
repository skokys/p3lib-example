const request = require("supertest");
const app = require("./app.js");

describe("Test the root path", () => {
    test("It should response the GET method", done => {
        request(app)
            .post("/decode")
            .send("data=8e023300e5630000010001047a00000003041fd855000408589514394cd8040005026d0006025000080200008104501304008f")
            .then(response => {
                expect(response.statusCode).toBe(200);
                expect(response.body[0].msg).toEqual("PASSING")
                done();
            })
    });
});