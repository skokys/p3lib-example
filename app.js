const express = require('express')
const logger = require('morgan')

const decodeRoutes = require('./routes/decodeRoutes')

const app = express()
app.listen(3000)
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/decode', decodeRoutes);

module.exports = app;

console.log("Started on port 3000. See readme to call decoding API")