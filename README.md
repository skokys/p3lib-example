# P3lib usage example

This sample app demonstrates how to use [P3lib](https://www.npmjs.com/package/p3lib)

Install dependencies (once only):

    npm i

Test

    npm test

Running example:

    npm start

Decoding P3 binary data to JSON:

    curl -X POST http://localhost:3000/decode -d 'data=8e023300e5630000010001047a00000003041fd855000408589514394cd8040005026d0006025000080200008104501304008f'

